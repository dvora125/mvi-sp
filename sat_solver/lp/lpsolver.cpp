#include <gurobi_c++.h>
#include "lpsolver.hpp"

solution lpsolver::solve(const instance& inst)
{
	int n = inst.n;
	GRBEnv env(true);
	env.set(GRB_IntParam_LogToConsole, 0);
	env.set(GRB_StringParam_LogFile, "ilp-log.txt");
	env.start();
	GRBModel model(env);
	std::vector<GRBVar> xs;
	xs.push_back(GRBVar());
	for(int var = 1; var <= n; ++var)
		xs.push_back(model.addVar(0, 1, 0, GRB_BINARY));
	//std::vector<GRBVar> bs;
	GRBLinExpr obj = 0;
	for(const auto& clause: inst.clauses)
	{
		auto b = model.addVar(0, 1, 0, GRB_BINARY);
		obj += b;
		//bs.push_back(b);
		GRBLinExpr constr = 0;
		for(int literal: clause.literals)
			if(literal < 0)
			{
				constr += 1 - xs[-literal];
				model.addConstr(b + xs[-literal] >= 1);
			}
			else
			{
				constr += xs[literal];
				model.addConstr(b + 1 - xs[literal] >= 1);
			}
		
		//add constr b <=> C  (~b | C) & (~x | b) & (~y | b) & (~z | b)
		//assume C = x | y | z
		
		//model.addConstr(constr >= 1);
		model.addConstr(1 - b + constr >= 1);
	}
	model.setObjective(obj, GRB_MAXIMIZE);
	model.optimize();
	std::vector<unsigned char> assignment(n);
	for(int var = 1; var <= n; ++var)
		assignment[var - 1] = (unsigned char)xs[var].get(GRB_DoubleAttr_X);
	return solution {obj.getValue() >= inst.m, assignment, inst};
}
