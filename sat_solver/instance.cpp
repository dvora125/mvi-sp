#include "instance.hpp"

int instance::num_sat_clauses(const std::vector<unsigned char>& assignment) const
{
	int fit = 0;
	for(auto& clause: clauses)
		if(clause.is_true(assignment))
			++fit;
	return fit;
}