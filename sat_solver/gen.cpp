#include <bits/stdc++.h>

using namespace std;

//fast ugly generator
//won't repeat clauses,
int main(int argc, char* argv[])
{
	if(argc < 3)
	{
		fprintf(stderr, "usage: %s <n> <m> [sat=n] [seed=time] [k=3]\n", argv[0]);
		return 1;
	}
	int n = atoi(argv[1]);
	int m = atoi(argv[2]);
	bool sat = argc >= 4 && strcasecmp(argv[3], "y") == 0;
	int seed = argc >= 5 ? atoi(argv[4]) : time(nullptr);
	int k = argc >= 6 ? atoi(argv[5]) : 3;
	if(n <= 0 || m <= 0 || k <= 0)
	{
		fprintf(stderr, "invalid n, m, k\n");
		return 1;
	}
	
	srand(seed);
	
	vector<bool> my_sat(n + 1);
	for(int i = 1; i <= n; ++i)
		my_sat[i] = rand() & 1;
	
	cout << "p cnf " << n << " " << m << '\n';
	
	set<set<int>> clauses;
	
	
	while((int)clauses.size() != m)
	{
		set<int> c;
		if(sat)
		{
			int lit = 1 + (rand() % n);
			c.insert(my_sat[lit] ? lit : -lit);
		}
		while(c.size() != k) //k-sat
			c.insert((rand() & 1 ? 1 : -1) * (1 + (rand() % n)));
		clauses.insert(move(c));
	}
	
	for(auto it = clauses.begin(); it != clauses.end(); ++it, cout << "0\n")
		for(auto jt = it->begin(); jt != it->end(); ++jt, cout << ' ')
			cout << *jt;
}
