#pragma once

#include <istream>
#include "instance.hpp"

class instance_reader
{
 public:
	virtual ~instance_reader() = default;
	virtual instance read_instance(std::istream& is) = 0;
};