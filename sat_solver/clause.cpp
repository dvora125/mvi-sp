#include "clause.hpp"

bool clause::is_true(const std::vector<unsigned char>& assignment) const
{
	for(auto& literal: literals)
		if(is_true(literal, assignment))
			return true;
	return false;
}
bool clause::is_true(int literal, const std::vector<unsigned char>& assignment)
{
	return literal < 0 ? !assignment[-literal-1] : assignment[literal-1];
}
bool clause::is_all_neg() const
{
	for(auto& literal: literals)
		if(literal > 0)
			return false;
	return true;
}

bool clause::is_all_pos() const
{
	for(auto& literal: literals)
		if(literal < 0)
			return false;
	return true;
}
