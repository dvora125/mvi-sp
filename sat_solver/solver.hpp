#pragma once

#include <atomic>
#include "solution.hpp"
#include "instance.hpp"

class solver
{
 public:
	virtual ~solver() = default;
	virtual solution solve(const instance& instance) = 0;
};
