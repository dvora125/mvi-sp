#pragma once

#include <random>

class rng
{
 public:
	using result_type = std::mt19937::result_type;
	result_type operator ()(result_type lo, result_type hi);
	result_type operator ()();
	static rng& get()
	{
		static rng instance;
		return instance;
	}
	std::mt19937& base();
	rng(const rng&) = delete;
	rng(rng&&) = delete;
	void operator =(const rng&) = delete;
	void operator =(rng&&) = delete;
 private:
	rng() = default;
	std::mt19937 m_rng {std::random_device {}()};
};







