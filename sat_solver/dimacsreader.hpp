#pragma once

#include <sstream>
#include <cassert>
#include "instance_reader.hpp"

class dimacs_reader : instance_reader
{
 public:
	instance read_instance(std::istream& is) override
	{
		auto ls = get_uc_line(is);
		std::string p, cnf;
		int n = 0, m = 0;
		ls >> p >> cnf >> n >> m;
		assert(ls);
		assert(p == "p");
		assert(cnf == "cnf");
		instance inst {n, m};
		for(int i = 0; i < m; ++i)
		{
			ls = get_uc_line(is);
			int literal;
			clause c;
			while(ls >> literal && literal != 0)
			{
				assert(-n <= literal && literal <= n);
				c.literals.push_back(literal);
			}
			inst.clauses.push_back(std::move(c));
		}
		return inst;
	}
 private:
	std::stringstream get_uc_line(std::istream& is)
	{
		std::stringstream ss;
		std::string line;
		do getline(is, line); while(line[0] == 'c');
		return std::stringstream(std::move(line));
	}
};