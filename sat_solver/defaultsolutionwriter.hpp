#pragma once


#include "solution_writer.hpp"

class default_solution_writer : public solution_writer
{
 public:
	void output_solution(const solution &solution, std::ostream &os) override;
};




