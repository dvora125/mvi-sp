#include <iostream>
#include <memory>
#include <cstring>
#include "dimacsreader.hpp"
#include "gen_alg/genalgsolver.hpp"

#include "defaultsolutionwriter.hpp"
#include "lp/lpsolver.hpp"
#include "timer.hpp"

int main(int argc, char** argv)
{
	dimacs_reader rd;
	instance inst = rd.read_instance(std::cin);
	
	std::unique_ptr<solver> s;
	if(argc >= 2 && strcasecmp(argv[1], "lp") == 0)
		s = std::unique_ptr<solver>(new lpsolver {});
	else if(argc >= 2 && strcasecmp(argv[1], "1") == 0)
		s = std::unique_ptr<solver>(new gen_alg_solver(&unif_crossover,
													   &multibit_mutation<decltype(&fitness)>,
													   &fitness));
	else if(argc >= 2 && strcasecmp(argv[1], "2") == 0)
		s = std::unique_ptr<solver>(new gen_alg_solver(&tp_crossover,
													   &flipGA<decltype(&fitness)>,
													   &fitness));
	else if(argc >= 2 && strcasecmp(argv[1], "3") == 0)
		s = std::unique_ptr<solver>(new gen_alg_solver(&tp_crossover,
													   &flipGA<decltype(&fitness)>,
													   &fitness, 5));
	else
	{
		
		std::cerr << "Enter method {1, 2, 3, lp} " << std::endl;
		return 1;
	}
	
	
	
	timer t;
	t.start();
	solution solution = s->solve(inst);
	t.end();
	auto time = t.time();
	std::stringstream ss;
	default_solution_writer wr;
	wr.output_solution(solution, ss);
	std::cout << ss.str() << "," << time << std::endl;
	return 0;
}
