#pragma once

#include <algorithm>
#include "../solver.hpp"
#include "../rng.hpp"

using individual_t = std::vector<unsigned char>;
using population_t = std::vector<std::pair<int, individual_t>>;
void sp_crossover(individual_t& a, individual_t& b);
void tp_crossover(individual_t& a, individual_t& b);
void unif_crossover(individual_t& a, individual_t& b);
template<typename FitnessFn>
void singlebit_mutation(individual_t& a, FitnessFn, const instance&)
{
	size_t i = rng::get()(0, a.size() - 1);
	a[i] = !a[i];
}
template<typename FitnessFn>
void multibit_mutation(individual_t& a, FitnessFn, const instance&)
{
	size_t num_flips = rng::get()(1, a.size());
	for(size_t i = 0; i < num_flips; ++i)
	{
		size_t idx = rng::get()(0, a.size() - 1);
		a[idx] = !a[idx];
	}
}

template<typename FitnessFn>
void flipGA(individual_t& a, FitnessFn fitness, const instance& inst)
{
	bool changed = true;
	while(changed)
	{
		changed = false;
		auto best_fit = fitness(inst, a);
		for(size_t i = 0; i < a.size(); ++i)
		{
			a[i] = !a[i];
			auto new_fit = fitness(inst, a);
			if(new_fit > best_fit)
			{
				changed = true;
				best_fit = new_fit;
			}
			else
				a[i] = !a[i];
		}
	}
}
int fitness(const instance& instance, individual_t& a);
individual_t random_assignment(size_t n);


template<typename CrossoverFn, typename MutationFn, typename FitnessFn>
class gen_alg_solver : public solver
{
 public:
	gen_alg_solver(CrossoverFn crossover, MutationFn mutation, FitnessFn fitness, size_t restarts = 1) : restarts(restarts),
																										 crossover(std::move(crossover)),
																										 mutation(std::move(mutation)),
																										 fitness(std::move(fitness)) { }
	
	
	solution solve(const instance& instance) override
	{
		individual_t best_assignment;
		int max_fitness = -1;
		
		for(size_t r = 0; r < restarts; ++r)
		{
			population_t population;
			for(size_t i = 0; i < pop_size; ++i)
				population.emplace_back(-1, random_assignment(instance.n));
			
			for(size_t iter = 0; iter < max_iter / restarts; ++iter)
			{
				for(auto& ind: population)
				{
					if(ind.first == -1)
						ind.first = fitness(instance, ind.second);
					
					if(ind.first == instance.m)
						return {true, ind.second, instance};
					
					if(ind.first > max_fitness)
					{
						max_fitness = ind.first;
						best_assignment = ind.second;
					}
				}
				
				std::sort(population.begin(), population.end(),
						  [](const std::pair<int, individual_t>& a, const std::pair<int, individual_t>& b) {
							  return a.first > b.first;
						  });
				population.resize((size_t)(elitism_rate * population.size()));
				roullete_wheel_fill(instance, population);
			}
		}
		return {false, best_assignment, instance};
	}
	
	const size_t restarts = 1;
	const double elitism_rate = 0.7;
	const double crossover_rate = 1;
	const double mutation_rate = 0.5;
	const size_t pop_size = 100;
	const size_t max_iter = 500;
	
	void roullete_wheel_fill(const instance& instance, population_t& population)
	{
		if(population.size() == pop_size)
			return;
		std::vector<int> prefix(pop_size);
		prefix[0] = population[0].first;
		for(size_t i = 1; i < population.size(); ++i)
			prefix[i] = prefix[i - 1] + population[i].first;
		while(population.size() < pop_size)
		{
			auto get_random_index = [&]()->size_t {
				int r = rng::get()(1, prefix[population.size() - 1]);
				auto it = std::lower_bound(prefix.begin(), prefix.begin() + population.size(), r);
				if(it == prefix.begin() + population.size())
					return 0;
				else
					return it - prefix.begin();
			};
			size_t i = get_random_index();
			size_t j = get_random_index();
			
			individual_t ith = population[i].second;
			individual_t jth = population[j].second;
			
			auto create_new_individual = [&]()->std::pair<int, individual_t> {
				std::uniform_real_distribution<> distribution(0.0, 1.0);
				if(distribution(rng::get().base()) < crossover_rate)
					crossover(ith, jth);
				if(distribution(rng::get().base()) < mutation_rate)
					mutation(ith, fitness, instance);
				if(distribution(rng::get().base()) < mutation_rate)
					mutation(jth, fitness, instance);
				auto ifit = fitness(instance, ith);
				auto jfit = fitness(instance, jth);
				std::pair<int, individual_t> ii {ifit, std::move(ith)};
				std::pair<int, individual_t> jj {jfit, std::move(jth)};
				if(ii.first > jj.first)
					return ii;
				else
					return jj;
			};
			population.push_back(create_new_individual());
			prefix[population.size() - 1] = prefix[population.size() - 2] + population.back().first;
		}
	}
	
	CrossoverFn crossover;
	MutationFn mutation;
	FitnessFn fitness;
};

