#include <algorithm>
#include "genalgsolver.hpp"

void sp_crossover(std::vector<unsigned char>& a, std::vector<unsigned char>& b)
{
	size_t i = rng::get()(0, a.size() - 1);
	for(; i < a.size(); ++i)
		std::swap(a[i], b[i]);
}
void tp_crossover(std::vector<unsigned char>& a, std::vector<unsigned char>& b)
{
	size_t i = rng::get()(0, a.size() - 1);
	size_t j = rng::get()(0, a.size() - 1);
	if(i > j)
		std::swap(i, j);
	for(; i <= j; ++i)
		std::swap(a[i], b[i]);
}

void unif_crossover(std::vector<unsigned char>& a, std::vector<unsigned char>& b)
{
	for(size_t i = 0; i < a.size(); ++i)
		if(a[i] != b[i] && rng::get()(0, 1) == 1)
			std::swap(a[i], b[i]);
}
int fitness(const instance& instance, std::vector<unsigned char>& a)
{
	return instance.num_sat_clauses(a);
}
std::vector<unsigned char> random_assignment(size_t n)
{
	std::vector<unsigned char> a;
	a.reserve(n);
	for(size_t i = 0; i < n; ++i)
		a.push_back(rng::get()(0, 1));
	return a;
}
