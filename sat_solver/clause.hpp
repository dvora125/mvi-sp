#pragma once

#include <vector>

struct clause
{
	bool is_true(const std::vector<unsigned char>& assignment) const;
	static bool is_true(int literal, const std::vector<unsigned char>& assignment);
	bool is_all_neg() const;
	bool is_all_pos() const;
	std::vector<int> literals;
};




