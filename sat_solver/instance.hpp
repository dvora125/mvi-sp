#pragma once

#include <vector>
#include "clause.hpp"

struct instance
{
	instance(int n, int m, std::vector<clause> clauses = std::vector<clause> {}) : n(n), m(m), clauses(std::move(clauses)) { }
	int num_sat_clauses(const std::vector<unsigned char>& assignment) const;
	int n, m;
	std::vector<clause> clauses;
};
