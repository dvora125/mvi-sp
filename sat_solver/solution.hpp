#pragma once

#include <vector>
#include "instance.hpp"

struct solution
{
	bool sat;
	std::vector<unsigned char> assignment;
	instance inst;
};