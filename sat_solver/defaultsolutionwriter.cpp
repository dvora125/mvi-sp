#include <iostream>
#include "defaultsolutionwriter.hpp"

void default_solution_writer::output_solution(const solution& solution, std::ostream& os)
{
	os << solution.inst.num_sat_clauses(solution.assignment);
}
