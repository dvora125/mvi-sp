#pragma once

#include <ostream>
#include "solution.hpp"

class solution_writer
{
 public:
	virtual ~solution_writer() = default;
	virtual void output_solution(const solution& solution, std::ostream& os) = 0;
};