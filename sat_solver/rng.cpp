#include "rng.hpp"

rng::result_type rng::operator ()(rng::result_type lo, rng::result_type hi)
{
	return lo + operator ()() % (hi - lo + 1);
}
rng::result_type rng::operator ()()
{
	return m_rng();
}
std::mt19937& rng::base()
{
	return m_rng;
}
